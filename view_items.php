<?php
include 'header.php';
include 'viewactive.php';
include 'php/dbconnection.php';
$query = "select * from item";
$result = $conn->query($query);
echo "
<div class=\"col col-lg-6\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Item name</th>
				<th>Category</th>
				<th>Unit</th>
				<th>Buying Price</th>
				<th>Selling Price</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>";
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$cat = $row["categoryid"];
				$query1 = "select category_name from category where categoryid = '$cat'";
				$result1 = $conn->query($query1);
				$row1 = $result1->fetch_assoc();
				echo "<tr>
				<td>".$row["item_name"]."</td>
				<td>".$row1["category_name"]."</td>
				<td>".$row["unit"]."</td>
				<td>".$row["buying_price"]."</td>
				<td>".$row["printed_price"]."</td>
				<td><a href=\"update_item.php?item=".$row["item_name"]."\" class=\"btn btn-info\" role=\"button\">Update</a></td>
				<td><a href=\"php/delete_db.php?item=".$row["item_name"]."\" class=\"btn btn-info\" role=\"button\">Delete</a></td>
				</tr>";
			}
		}
		else{
			echo "<tr><td>No items added.<td><tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>
"
?>
