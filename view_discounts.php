<?php
include 'header.php';
include 'viewactive.php';
include 'php/dbconnection.php';

$query = "select item_name, discount, start_date, end_date from item, discount where discount.itemid = item.itemid";
$result = $conn->query($query);


echo "
<div class=\"container\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Item name</th>
				<th>Discount</th>
				<th>From</th>
				<th>To</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>";

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				echo "<tr>
				<td>".$row["item_name"]."</td>
				<td>".$row["discount"]."%</td>
				<td>".$row["start_date"]."</td>
				<td>".$row["end_date"]."</td>
				<td><a href=\"update_discount.php?discount=".$row["item_name"]."\" class=\"btn btn-info\" role=\"button\">Update</a></td>
				<td><a href=\"php/delete_db.php?discount=".$row["item_name"]."\" class=\"btn btn-info\" role=\"button\">Delete</a></td>
				</tr>";
			}
		}
		else{
			echo "<tr><td>No discount added.<td><tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>

"
?>
