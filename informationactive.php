<?php

echo"
	<nav class=\"navbar navbar-light navbar-expand-sm bg-faded\">
	
	<ul class=\"navbar-nav\">
		<li class=\"nav-item\">
		<a class=\"nav-link\" href=\"seller.php\">Home</a>
		</li>
		<li class=\"nav-item\">
		<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"Aministration\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Administration
		</a>
		<div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"Aministration_drops\">
		<a class=\"dropdown-item\" href=\"addcategory.php\">Add Item Category</a>
		<a class=\"dropdown-item\" href=\"additem.php\">Add Item</a>
		<a class=\"dropdown-item\" href=\"addbatch.php\">Add Batch</a>
		<a class=\"dropdown-item\" href=\"adddiscount.php\">Add Discount</a>
		<a class=\"dropdown-item\" href=\"addwholeseller.php\">Add Wholeseller</a>
		</div>
		</li>
		<li class=\"nav-item active\">
		<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"Reports\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Information
		</a>
		<div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"Report_drops\">
		<a class=\"dropdown-item\" href=\"info_orders.php\">Orders</a>
		<a class=\"dropdown-item\" href=\"info_orderuser.php\">Orders by a user</a>
		<a class=\"dropdown-item\" href=\"info_ordercat.php\">Orders of a category</a>
		<a class=\"dropdown-item\" href=\"info_orderitem.php\">Orders of an item</a>
		<a class=\"dropdown-item\" href=\"finish.php\">Items about to finish</a>
		</div>
		</li>
		<li class=\"nav-item\">
		<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"View\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Views</a>
		<div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"view_drops\">
		<a class=\"dropdown-item\" href=\"view_wholeseller.php\">Wholesellers</a>
		<a class=\"dropdown-item\" href=\"view_categories.php\">Categories</a>
		<a class=\"dropdown-item\" href=\"view_items.php\">Items</a>
		<a class=\"dropdown-item\" href=\"view_discounts.php\">Discounts</a>
		</div>
		</li>

	</ul>
</nav>
<div class=\"col-lg-12\" id=\"seperation\"></div>
";

?>
