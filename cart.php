<?php
include 'php/dbconnection.php';
include 'header.php';
include 'cartactive.php';
static $subtotal = 0;
$item_name=$_GET['item'];
$left=$_GET['left'];
$query="select * from item where item_name='$item_name'";
$result=$conn->query($query);
$row=$result->fetch_assoc();
$query2="select cartid from cart where userid='".$_SESSION['userid']."'";
$result2=$conn->query($query2);
$row2=$result2->fetch_assoc();
$cartid=$row2["cartid"];
$query3="select *from products where cartid='$cartid'";
$result3=$conn->query($query3);
$count=mysqli_num_rows($result3);
$action=isset($_GET['action']) ? $_GET['action'] : "";
echo "
<div class=\"container\" id=\"itemTable\">";
if($action=='removed'){
	echo "<div class='alert alert-info'>";
		echo "Product was removed from your cart!";
	echo "</div>";
	}
 
	else if($action=='add'){
	echo "<div class='alert alert-info'>";
		echo "Item added to cart!";
	echo "</div>";
	}
 
	else if($action=='exists'){
	echo "<div class='alert alert-info'>";
		echo "Product already exists in your cart!";
	echo "</div>";
	}
 
	else if($action=='cart_emptied'){
	echo "<div class='alert alert-info'>";
		echo "Cart was emptied.";
	echo "</div>";
	}
 
	else if($action=='updated'){
	echo "<div class='alert alert-info'>";
		echo "Quantity was updated.";
	echo "</div>";
	}
 
	else if($action=='unable_to_update'){
	echo "<div class='alert alert-danger'>";
		echo "Unable to update quantity.";
	echo "</div>";
	}
	else if($action=='notinstock'){
	echo "<div class='alert alert-danger'>";
		echo "Only ".$left." in stock";
	echo "</div>";
	}
echo "<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Item</th>
				<th>Discount(%)</th>
				<th>Price</th>
				<th>In cart</th>
				<th>Quantity</th>
				<th>Unit</th>
				<th>Total</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>";
				if(mysqli_num_rows($result3) >= 0) {
				while($row=$result3->fetch_assoc()) {

				$item=$row["itemid"];
				$query5="select item_name, unit, printed_price from item where itemid='$item'";
				$result5=$conn->query($query5);
				$row5=$result5->fetch_assoc();

				$unit=$row5["unit"];
				$query1="select discount from discount where itemid='$item'";
				$result1=$conn->query($query1);
				$row1=$result1->fetch_assoc();
				$discount=$row1["discount"];
				if($discount == NULL) {
					$discount="-";
				}
				$name=$row5["item_name"];
				echo "<tr>
				<td>".$name."</td>
				<td>".$discount."</td>
				<td>₹";
				$quantity=1;
				echo $row5["printed_price"];
				echo "</td>";
				
				
				$query6 = "select quantity from products where itemid = '$item' and cartid = '$cartid'";
				$result6=$conn->query($query6);
				$row6=$result6->fetch_assoc();
				$price_quantity = $row6["quantity"];
				$total = ($price_quantity * $row5["printed_price"])-(($discount * $price_quantity * $row5["printed_price"])/100);
				$subtotal = $subtotal + $total;
				$query7 = "update cart set price = '$subtotal' where cartid = '$cartid'";
				$result7=$conn->query($query7);
				echo "<td>".$price_quantity."</td>";

				echo "<form class='update-quantity-form' action=\"update_quantity.php\" method='GET'>";
		echo "<div class='product-id' style='display:none;'>{$item}</div>";
		echo "<td>";
			echo "<input type='number' name='quantity' value='{$quantity}' class='form-control cart-quantity' min='1' />";
			echo "</td>";
			
			echo "<td>per ".$unit."</td>";
			echo "<td>₹".$total."</td>";
			echo"<td>";
			echo "<input type=\"hidden\" value=\"{$item}\" name=\"id\"/>";
				echo "<input class=\"btn btn-info\" type=\"submit\"name=\"update\"value=\"Update\"id=\"updatecart\"/>";
				
				echo "</td>";
			echo"<td>";
			echo "<a href=\"remove_from_cart.php?id={$item}&quantity={$quantity}\" class=\"btn btn-info\">";
			echo "Delete";
		echo "</a>";
		echo "</td>";
		echo "</form>";
		echo "</tr>";
		}
		echo "<tr>
				<td></td><td></td><td></td><td></td><td><td></td></td><td><b>₹".$subtotal."</b></td><td></td>
				<td>
					<a href=\"checkout.php\" class=\"btn btn-info\">Checkout</a>
				</td>
			</tr>";
		}
		else {
			echo "<tr>";
			echo "<td>";
			echo "No items in cart.";
			echo "</td>";
			echo "</tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>
";
?>
