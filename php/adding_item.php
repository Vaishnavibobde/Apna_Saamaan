<?php
include 'dbconnection.php';
include 'test_input.php';
if(isset($_POST['additem'])) {
	$item_name=test_input($_POST['item_name']);
	$category_name=test_input($_POST['category_name']);
	$unit = $_POST['unit'];
	$buying=test_input($_POST['buying']);
	$selling=test_input($_POST['selling']);
}
$queryi="select item_name from item where item_name='$item_name'";
$resulti=$conn->query($queryi);
$counti=mysqli_num_rows($resulti);
$query="select categoryid from category where category_name='$category_name'";
$result=$conn->query($query);
$count=mysqli_num_rows($result);
if(empty($item_name) or empty($category_name) or empty($unit) or empty($buying) or empty($selling)) {
	echo "<script type=\"text/javascript\">alert(\"Fill all the fields.\");
	window.location.replace(\"../additem.php\");
	</script>";	
}
else if($count==0) {
	echo "<script type=\"text/javascript\">alert(\"Invalid Category\");
	window.location.replace(\"../additem.php\");
	</script>";
}
else if ($counti != 0){
	echo "<script type=\"text/javascript\">alert(\"Item already exists\");
	window.location.replace(\"../additem.php\");
	</script>";

}
else if($count==1){
	$row=$result->fetch_assoc();
	$categoryid=$row["categoryid"];
	$query="insert into item (item_name, categoryid, unit, buying_price, printed_price) values ('$item_name', '$categoryid', '$unit', '$buying', '$selling')";
	$result1=$conn->query($query);
	if($result === false){
		echo "<script type=\"text/javascript\">alert(\"Item not added. Please try again!\");
			window.location.replace(\"../additem.php\");
			</script>";
		die("Query $query returned false");
	}
	else{
		echo "<script type=\"text/javascript\">
			window.location.replace(\"../additem.php\");
			</script>";
	}
}
else{
	echo "<script type=\"text/javascript\">alert(\"Error occured. Please try again.\");
	window.location.replace(\"../additem.php\");
	</script>";
}
?>
