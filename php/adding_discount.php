<?php
include 'dbconnection.php';
include 'test_input.php';

if(isset($_POST['add_discount'])) {
$discountper = test_input($_POST['discount%']);
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$item_name = test_input($_POST['item_name']);
}

$query1 = "select itemid from item where item_name = '$item_name'";
$result1 = $conn->query($query1);
$count1 = mysqli_num_rows($result1);


if(empty($discountper) or empty($startdate) or empty($enddate) or empty($item_name)) {
echo "<script type=\"text/javascript\">alert(\"Fill all the fields.\");
window.location.replace(\"../adddiscount.php\");
</script>";
}

else if($count1 == 0) {
	echo "<script type=\"text/javascript\">alert(\"Item doesn't exist.\");
window.location.replace(\"../adddiscount.php\");
</script>";
}

else if(strtotime($startdate) > strtotime($enddate)) {
	echo "<script type=\"text/javascript\">alert(\"Discount should start before it ends.\");
window.location.replace(\"../adddiscount.php\");
</script>";
}

else if($count1 == 1) {
	$row1 = $result1->fetch_assoc();
	$itemid = $row1["itemid"];
	
	$query2 = "select discount from discount where itemid = '$itemid'";
	$result2 = $conn->query($query2);
	$count2 = mysqli_num_rows($result2);
	echo $count2;
	if($count2 == 1) {
	echo $count2;
		$query4 = "update discount set discount = '$discountper', start_date = '$startdate', end_date = '$enddate' where itemid = '$itemid'";
		$result4 = $conn->query($query4);
		if($result4 === false)  {
	echo "<script type=\"text/javascript\">alert(\"Discount not added. Please try again!\");
window.location.replace(\"../adddiscount.php\");
</script>";
        die("Query $query returned false");
    	}
	else {
		
		echo "<script type=\"text/javascript\">alert(\"Discount successfully updated!\");
window.location.replace(\"../adddiscount.php\");
</script>";
	}
	}
	else if($count2 == 0) {
	$query3 = "insert into discount (start_date, end_date, discount, itemid) values ('$startdate', '$enddate', '$discountper', '$itemid')";
	$result3 = $conn->query($query3);
	if($result3 === false)  {
	echo "<script type=\"text/javascript\">alert(\"Discount not added. Please try again!\");
window.location.replace(\"../adddiscount.php\");
</script>";
        die("Query $query returned false");
    	}
	else {
		
		echo "<script type=\"text/javascript\">alert(\"Discount successfully added!\");
window.location.replace(\"../adddiscount.php\");
</script>";
	}
	}
}
else{
	echo "<script type=\"text/javascript\">alert(\"Error occured. Please try again.\");
	window.location.replace(\"../adddiscount.php\");
	</script>";
}
