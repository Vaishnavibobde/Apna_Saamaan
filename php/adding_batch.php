<?php
include 'dbconnection.php';
include 'test_input.php';
if(isset($_POST['addbatch'])) {
	$item=test_input($_POST['item']);
	$wholeseller=test_input($_POST['wholeseller']);
	$quantity=test_input($_POST['quantity']);
	//$buying=test_input($_POST['buying']);
	//$selling=test_input($_POST['selling']);
	$inward= $_POST['inward'];
	$expiry= $_POST['expiry'];
	$description=test_input($_POST['description']);
}

$queryc="select wholesellerid from wholeseller where name='$wholeseller'";
$resultc=$conn->query($queryc);
$countc=mysqli_num_rows($resultc);
$queryi="select itemid  from item where item_name='$item'";
$resulti=$conn->query($queryi);
$counti=mysqli_num_rows($resulti);

if(empty($wholeseller) or empty($item) or empty($quantity) or empty($expiry) or empty($inward)) {

	echo "<script type=\"text/javascript\">alert(\"Fill all the fields.\");
	window.location.replace(\"../addbatch.php\");
	</script>";
}
else if($countc==0){

	echo "<script type=\"text/javascript\">alert(\"Invalid Wholeseller\");
	window.location.replace(\"../addbatch.php\");
	</script>";
}
else if($counti==0){

	echo "<script type=\"text/javascript\">alert(\"Invalid Item\");
	window.location.replace(\"../addbatch.php\");
	</script>";
}
else if(strtotime($inward) > strtotime($expiry)) {
	echo "<script type=\"text/javascript\">alert(\"Expired items not excepted.\");
window.location.replace(\"../addbatch.php\");
</script>";
}
else if(($countc==1) && ($counti==1)){
	$rowc=$resultc->fetch_assoc();
	$wholesellerid=$rowc["wholesellerid"];
	$rowi=$resulti->fetch_assoc();
	$itemid=$rowi["itemid"];
	$query="insert into batch (expiry_date, inward_date, quantity, description, itemid, wholesellerid) values ('$expiry', '$inward', '$quantity', '$description', '$itemid', '$wholesellerid')";
	
	$result=$conn->query($query);
	
	if($result === false){
		echo "<script type=\"text/javascript\">alert(\"Batch ID already exists.\");
		window.location.replace(\"../addbatch.php\");
		</script>";
		die("Query $query returned false");
	}
	else{
		$query1 = "select batchid from batch where expiry_date = '$expiry' and inward_date = '$inward' and quantity = '$quantity' and itemid = '$itemid' and wholesellerid = '$wholesellerid'";
		$result1=$conn->query($query1);
		$row1=$result1->fetch_assoc();
		$batchid = $row1["batchid"];
		$query = "insert into inventory (left_quantity, batchid) values ('$quantity', '$batchid')";
		$result = $conn->query($query);
		if($result === false){
			echo "<script type=\"text/javascript\">alert(\"Item quantity not updated\");
			window.location.replace(\"../addbatch.php\");
			</script>";
			die("Query $query returned false");
		}
		else {
		echo "<script type=\"text/javascript\">alert(\"Successfully Added\");
			window.location.replace(\"../addbatch.php\");
			</script>";
		}	
	}
}
else{
	echo "<script type=\"text/javascript\">alert(\"Error occured. Please try again.\");
	window.location.replace(\"../addbatch.php\");
	</script>";
}
?>
