<?php
session_start();
$username=$_SESSION['user_name'];
echo "
<!doctype html>
<html>
<head>
<title>Apna Saamaan</title>
<meta charset=\"utf-8\"/>
<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\"></script>
<script src=\"js/bootstrap.min.js\"></script>
<link rel=\"stylesheet\" href=\"css/styling.css\">
<link rel=\"icon\" href=\"as.ico\" type=image/x-icon>
</head>
<body>
	<div class=\"row\" id=\"header\">
		<div class=\"col-lg-8\">
		<h1>Apna Saamaan</h1></div>
		<div class=\"col-lg-4 text-right\">
			<br>Logged in as
			<a class=\"dropdown dropdown-toggle\" href=\"#\" id=\"Account\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">".$username."</a>
			<div class=\"dropdown-menu\" aria-labelledby=\"Account_drops\">
				<a class=\"dropdown-item\" href=\"change_passwd.php\">Change Password</a>
				<a class=\"dropdown-item\" href=\"php/logout.php\">Logout</a>
			</div>
	</div>	
"
?>
