<?php
include 'header.php';
include 'viewactive.php';
include 'php/dbconnection.php';

$query = "select * from category";
$result = $conn->query($query);


echo "
<div class=\"col col-lg-4\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Categories</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>";

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				echo "<tr>
				<td>".$row["category_name"]."</td>
				<td><a href=\"update_category.php?category=".$row["category_name"]."\" class=\"btn btn-info\" role=\"button\">Update</a></td>
				<td><a href=\"php/delete_db.php?category=".$row["category_name"]."\" class=\"btn btn-info\" role=\"button\">Delete</a></td>
				</tr>";
			}
		}
		else{
			echo "<tr><td>No categories added.<td></tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>

"
?>
