<?php
include 'php/dbconnection.php';
include 'header.php';
include 'informationactive.php';
$userid=$_GET['userid'];
$query="select * from transaction where userid='$userid' order by time desc";
$result=$conn->query($query);
if($result == false)	echo "false";
if ($result->num_rows > 0) {
	while($row=$result->fetch_assoc()) {
	$transactionid=$row['transactionid'];
	$query1="select * from transproducts where transactionid='$transactionid'";
	$result1=$conn->query($query1);
		echo "<div class=\"container\" id=\"productsTable\">";
			echo "<h3>".$row['time']."</h3>";
			echo "<table class=\"table table-hover\">
				<thead>
					<tr>
						<th>Item name</th>
						<th>Discount(%)</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>";
				if ($result1->num_rows > 0) {
					while($row1=$result1->fetch_assoc()) {
						$itemid=$row1['itemid'];
						$query2="select item_name, printed_price from item where itemid='$itemid'";
						$result2=$conn->query($query2);
						$row2=$result2->fetch_assoc();
						$query3="select discount from discount where itemid='$itemid'";
						$result3=$conn->query($query3);
						$row3=$result3->fetch_assoc();
						$discount=$row3["discount"];
						$price=$row2["printed_price"];
						if($discount==NULL) {
							$discount=0;
						}
						$quantity=$row1["quantity"];
						$subtotal=($quantity * $price) - ($discount * $quantity * $price)/ 100;
						echo "<tr>
							<td>".$row2["item_name"]."</td>
							<td>".$discount."</td>
							<td>₹".$row2["printed_price"]."</td>
							<td>".$quantity."</td>
							<td>₹".$subtotal."</td>
						</tr>";
					}
				}
				else{
					echo "<tr><td>No items added.<td><tr>";
				}
				echo "<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><b>₹".$row['price']."</b></td>";
				echo "</tbody>
			</table>";
	}
}
echo "</div>
</body>
</html>
"
?>
