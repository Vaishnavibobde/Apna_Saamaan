<?php
include 'php/dbconnection.php';
include 'header.php';
include 'homeactive.php';

$query = "select * from category;";
$categories=$conn->query($query);

echo "
<div class=\"container\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Categories of products available</th>
			</tr>
		</thead>
		<tbody>";

		if ($categories->num_rows > 0) {
			while($row = $categories->fetch_assoc()) {
				echo "<tr>
				<td><a href=\"add_to_cart.php?category=".$row["category_name"]."\">".$row["category_name"]."</a></td>
				</tr>";
			}
		}
		else{
			echo "<tr><td>No Categories of products available</td></tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>"
?>
