<?php
include 'php/dbconnection.php';
include 'header.php';
include 'informationactive.php';
$cat=$_GET['cat'];
$query4="select categoryid from category where category_name='$cat'";
$result4=$conn->query($query4);
$row4=$result4->fetch_assoc();
$categoryid=$row4['categoryid'];
$query5="select itemid from item where categoryid='$categoryid'";
$result5=$conn->query($query5);
echo "<div class=\"container\" id=\"productsTable\">";
echo "<table class=\"table table-hover\">
			<thead>
				<tr>
					<th>Item name</th>
					<th>Discount(%)</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>";
if ($result5->num_rows > 0) {
	while($row5=$result5->fetch_assoc()) {
		$query1="select * from transproducts where itemid='$itemid'";
		$result1=$conn->query($query1);
		$row1=$result1->fetch_assoc();
				
				$itemid=$row5['itemid'];
							$query2="select item_name, printed_price from item where itemid='$itemid'";
							$result2=$conn->query($query2);
							$row2=$result2->fetch_assoc();
							$query3="select discount from discount where itemid='$itemid'";
							$result3=$conn->query($query3);
							$row3=$result3->fetch_assoc();
							$discount=$row3["discount"];
							$price=$row2["printed_price"];
							if($discount==NULL) {
								$discount=0;
							}
							$quantity=$row1["quantity"];
							if($quantity == null) $quantity = 0;
							$subtotal=($quantity * $price) - ($discount * $quantity * $price)/ 100;
							echo "<tr>
								<td>".$row2["item_name"]."</td>
								<td>".$discount."</td>
								<td>₹".$row2["printed_price"]."</td>
								<td>".$quantity."</td>
								<td>₹".$subtotal."</td>
							</tr>";
						}
					}
					else{
						echo "<tr><td>No items added.<td><tr>";
					}
					echo "</tbody>
				</table>";
echo "</div>
</body>
</html>
"
?>
