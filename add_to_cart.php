<?php
include 'php/dbconnection.php';
include 'header.php';
include 'categoryactive.php';
$category=$_GET['category'];
$query="select categoryid from category where category_name = '$category'";
$result=$conn->query($query);
$row=$result->fetch_assoc();
$categoryid=$row['categoryid'];
$query = "select * from item where categoryid = '$categoryid'";
$result=$conn->query($query);
$count=mysqli_num_rows($result);

$action = isset($_GET['action']) ? $_GET['action'] : "";


echo "
<div class=\"container\" id=\"itemTable\">";

	if($action=='exists'){
        	echo "<div class='alert alert-info'>";
           	echo "Product already exists in your cart!";
        	echo "</div>";
    	}

	echo"<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Item</th>
				<th>Discount(%)</th>
				<th>Price</th>
				<th>Unit</th>
				<th>Add</th>
			</tr>
		</thead>
		<tbody>";
		if ($count > 0){
			while($row = $result->fetch_assoc()){
				$item = $row["itemid"];
				$unit = $row["unit"];
				$query1="select discount from discount where itemid='$item'";
				$result1=$conn->query($query1);
				$row1=$result1->fetch_assoc();
				$discount = $row1["discount"];
				if($discount == NULL) {
					$discount = "-";
				}
				$item_name = $row["item_name"];
				echo "<tr>
				<td>".$item_name."</td>
				<td>".$discount."</td>
				<td>₹";

				$quantity = 1;
				echo $row["printed_price"];
				echo "<td>per ".$unit."</td>";
				echo"<td>";
				echo"<a href=\"adding_to_cart.php?item=".$item_name."\" class=\"btn btn-info\" role=\"button\">Add to cart</a>";
				echo "</tr>";
		}
	}
	else{
		echo "<tr><td>No items available</td></tr>";
}
echo "</tbody>
</table>
</div>
</body>
</html>
";

?>
