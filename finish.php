<?php
include 'header.php';
include 'viewactive.php';
include 'php/dbconnection.php';
$query = "select * from item";
$result = $conn->query($query);

echo "
<div class=\"col col-lg-6\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Item name</th>
				<th>Unit</th>
				<th>Buying Price</th>
				<th>Selling Price</th>
				<th>Remaining</th>
			</tr>
		</thead>
		<tbody>";
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$total_quant = 0;
				$itemid = $row["itemid"];
				
				$query3 = "select batchid from batch where itemid = '$itemid'";
				$result3 = $conn->query($query3);
	
				while ($row3 = $result3->fetch_assoc()) {
				$batchid = $row3["batchid"];
				$query4 = "select left_quantity from inventory where batchid = '$batchid'";
				$result4=$conn->query($query4);
				$row4=$result4->fetch_assoc();
				$inbatch = $row4["left_quantity"];
				$total_quant = $total_quant + $inbatch;
				}
				
				if($total_quant < 10) {
			
				echo "<tr>
				<td>".$row["item_name"]."</td>
				<td>".$row["unit"]."</td>
				<td>₹".$row["buying_price"]."</td>
				<td>₹".$row["printed_price"]."</td>
				<td>".$total_quant."</td>
				</tr>";
				
				}
			}
		}
		else{
			echo "<tr><td>Sufficient items in stock.<td><tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>
";
?>