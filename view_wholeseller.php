<?php
include 'header.php';
include 'viewactive.php';
include 'php/dbconnection.php';

$query = "select * from wholeseller";
$result = $conn->query($query);


echo "
<div class=\"container\" id=\"productsTable\">
	<table class=\"table table-hover\">
		<thead>
			<tr>
				<th>Name</th>
				<th>Address</th>
				<th>Phone</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>";

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				echo "<tr>
				<td>".$row["name"]."</td>
				<td>".$row["address"]."</td>
				<td>".$row["phone"]."</td>
				<td><a href=\"update_wholeseller.php?wholeseller=".$row["name"]."\" class=\"btn btn-info\" role=\"button\">Update</a></td>
				<td><a href=\"php/delete_db.php?wholeseller=".$row["name"]."\" class=\"btn btn-info\" role=\"button\">Delete</a></td>
				</tr>";
			}
		}
		else{
			echo "<tr><td>No wholeseller added.<td><tr>";
		}
		echo "</tbody>
	</table>
</div>
</body>
</html>

"
?>
