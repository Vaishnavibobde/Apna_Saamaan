<?php
include 'php/dbconnection.php';
include 'header.php';
include 'viewactive.php';
	if(isset($_GET['item'])){
		$item=$_GET['item'];
		$query="select * from item where item_name = '$item'";
		$result=$conn->query($query);
		$row=$result->fetch_assoc();
		$categoryid=$row['categoryid'];
		$query1="select category_name from category where categoryid='$categoryid'";
		$result1=$conn->query($query1);
		$row1=$result1->fetch_assoc();
	}
	else if(isset($_POST['update'])){
		$item_name=$_POST['item_name'];
		$query3="select itemid from item where item_name='$item_name'";
		$result3=$conn->query($query3);
		if(mysqli_num_rows($result3) != 0){
			echo "<script type=\"text/javascript\">alert(\"Item already exists.\");
				window.location.replace(\"view_items.php\");
				</script>";
			die();
		}
		$unit=$_POST['unit'];
		$category_name=$_POST['category_name'];
		$buying_price=$_POST['buying_price'];
		$printed_price=$_POST['printed_price'];
		$itemid=$_POST['itemid'];
		$query2="select categoryid from category where category_name='$category_name'";
		$result2=$conn->query($query2);
		if($result2===false){
			echo "<script type=\"text/javascript\">alert(\"Category not found.\");
				window.location.replace(\"view_items.php\");
				</script>";
		}
		$row2=$result2->fetch_assoc();
		$categoryid=$row2['categoryid'];
		$query="update item set item_name='$item_name', unit='$unit', categoryid='$categoryid', buying_price='$buying_price', printed_price='$printed_price' where itemid='$itemid'";
		$result=$conn->query($query);
		if($result===false){
			echo "<script type=\"text/javascript\">alert(\"Error occured while updating the data. Please try again.\");
		</script>";
		}
		echo "<script type=\"text/javascript\">window.location.replace(\"view_items.php\");
		</script>";
	}
	else{
		echo "Unknown error occured!";
	}
?>
	<div class="col-lg-4" id="changepasswd">
		<form action="update_item.php" method="POST">
				<label for="item_name">Item Name</label>
				<input type="text" id="item_name" name="item_name" value="<?php echo $row['item_name']?>">
				<label for="category_name">Category</label>
				<input type="text" id="category_name" name="category_name" value="<?php echo $row1['category_name']?>">
				<label for="unit">Unit</label>
				<select id="unit" name="unit">
					<option value=<?php echo $row['unit']?>><?php echo $row['unit']?></option> 
					<option value="unit">unit</option>
					<option value="kg">kg</option>
					<option value="litre">litre</option>
					<option value="dozen">dozen</option>
					<option value="gram">gram</option>
					<option value="ml">ml</option>
					<option value="packet">packet</option>
				</select>
				<label for="buying_price">Buying Price</label>
				<input type="text" id="buying_price" name="buying_price" value="<?php echo $row['buying_price']?>">
				<label for="printed_price">Selling Price</label>
				<input type="text" id="printed_price" name="printed_price" value="<?php echo $row['printed_price']?>">
				<input type="hidden" id="itemid" name="itemid" value="<?php echo $row['itemid']?>">
				<input type="submit" value="Update" name="update">
			</form>
	</div>
</div>
</body>
</html>
