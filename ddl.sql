drop database if exists Store;
create database if not exists Store;
use Store;

create table if not exists wholeseller (
	wholesellerid	int(11) not null auto_increment,
	name		varchar(128) not null,
	address		varchar(256),
	phone		varchar(10),
	primary key (wholesellerid)
);

create table if not exists category (
	categoryid	int(11) not null auto_increment,
	category_name	varchar(128) not null,
	primary key(categoryid)	
);

create table if not exists item (
	itemid		int(11) not null auto_increment,
	item_name	varchar(128) not null,
	categoryid	int(11) not null,
	unit		varchar(11) not null,
	buying_price	decimal(10, 2) not null,
	printed_price	decimal(10, 2) not null,
	primary key (itemid),
	foreign key (categoryid) references category (categoryid) on delete cascade
);

create table if not exists batch (
	batchid		int(11) not null auto_increment,
	expiry_date	date,
	inward_date	date,
	quantity	int(11),
	description	varchar(256),
	itemid		int(11) not null,
	wholesellerid	int(11) not null,
	primary key (batchid),
	foreign key (itemid) references item (itemid),
	foreign key (wholesellerid) references wholeseller (wholesellerid)
);

create table if not exists inventory (
	inventoryid	int(11) not null auto_increment,
	left_quantity	int(11),
	batchid		int(11) not null,
	primary key (inventoryid),
	foreign key (batchid) references batch (batchid) on delete cascade
);

create table if not exists users (
	userid		int(11) not null ,
	user_name	varchar(128) not null,
	password	varchar(512) not null,
	user_type	varchar(15) not null,
	address		varchar(256),
	phone		varchar(10),
	primary key (userid)	
);

create table if not exists discount (
	discountid	int(11) not null auto_increment,
	start_date	date,
	end_date	date,
	discount	decimal(5, 2),
	itemid		int(11) not null,
	primary key (discountid),
	foreign key (itemid) references item (itemid) on delete cascade
);

create table if not exists cart (
	cartid		int(11) not null auto_increment,
	userid		int(11) not null,
	price		decimal(20, 5),
	primary key(cartid),
	foreign key (userid) references users (userid) on delete cascade
);

create table if not exists products (
	itemid 	int(11) not null,
	cartid	int(11) not null,
	quantity	int(11),
	primary key (itemid, cartid),
	foreign key (itemid) references item(itemid),
	foreign key (cartid) references cart(cartid) on delete cascade
);

create table if not exists transaction (
	transactionid	int(11) not null auto_increment,
	time			timestamp not null default current_timestamp,
	userid		int(11) not null,
	price		decimal(20, 5),
	primary key (transactionid),
	foreign key (userid) references users (userid)
);

create table if not exists transproducts(
	transactionid	int(11) not null,
	itemid 		int(11) not null,
	quantity		int(11),
	primary key (transactionid, itemid),
	foreign key (itemid) references item(itemid),
	foreign key (transactionid) references transaction(transactionid) on delete cascade
);
insert into users values (100, 'seller', 'f899139df5e1059396431415e770c6dd', 'seller', 'pune', 0712228244);