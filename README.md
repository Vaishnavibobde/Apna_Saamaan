                Apna Saamaan - An Online Store
It is a web application for a general online store.
It is developed by Vaishnavi Bobde and Kajal Sanklecha as a project
for Web Systems and Technologies course of College of Engineering, Pune.

This application can be used by store managers to sell their products online
and keep a track of it. This will also allow the customers to buy their
products online.

Check "install.txt" for instructions regarding installation.